/*
 * ARM offline test / car parking system control code
 *
 * Data structures to represent vehicles, car park, and misc. declarations and
 * definitions
 *
 * Author: Attila Szasz
 */

#ifndef _CARPARK_H
#define _CARPARK_H

#include <stdint.h>

#define OWNERNAMELEN 128
#define MAXCARS 2048

enum
{
        TREE_SUCC,
        TREE_ALRDY,
        TREE_NOMEM
};

enum
{
        CP_SUCC,
        CP_NOSPACE,
        CP_ERR
};

/*
 * We represent a linear car park with a binary tree of allocated parking slots
 * and a linked list of free parking segments
 */
struct car_park {
        unsigned int capacity;                  /* the maximum number of vehilces the park can hold */
        unsigned int allocated_slots;           /* the current number of vehicles in the park */
        struct tree_node *tree_root;            /* pointer to the root of the tree storing vehicles */
        struct free_segment *free_segment_root; /* pointer to the first free line segment */
};

/*
 * Free parking slots are represented by a linked list of free segments in the
 * linear car park.
 */
struct free_segment {
        unsigned int start;                      /* the starting index of the line segment */
        unsigned int end;                        /* the ending index of the line segment   */
        struct free_segment *next;
        struct free_segment *prev;
};

/*
 * Vehicles are represented by this struct
 *
 * We assume a bijection between number plate IDs and 64 bit unsigned integers,
 * for example Hungarian registration IDs can be represented by taking the 6
 * significant char values of a string such as ABC123 and storing them in the 8
 * byte unsigned integral type.
 *
 * We also assume that vehicles arriving and exiting from the park conform to a
 * uniform probability distribution in terms of the IDs/number plates, so there
 * is no (less) need for a self-balancing tree structure for lookup.
 */
struct car_entry {
        uint64_t numberplate;
        char ownername[OWNERNAMELEN];
        time_t entered;
};

/*
 * Vehicles assigned to parking slots are stored in a binary tree along with
 * their index representing their location in the linear parking space.
 * The tree is indexed/keyed by the license ID/number plate.
 */
struct tree_node {
        uint64_t numberplate;                    /* the key: the license number of the vehicle */
        unsigned int index;                      /* position of the vehicle in the linear parking space */
        struct car_entry vehicle;                /* the actual vehicle parking in this slot */
        struct tree_node *left_p, *right_p;      /* child nodes */
};

#endif/*_CARPARK_H*/
