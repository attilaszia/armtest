CC=gcc
CFLAGS=-Wall
EXECUTABLE=carpark

$(EXECUTABLE): carpark.c carpark.h
	$(CC) $(CFLAGS) -o $@ carpark.c

.PHONY: run
run: carpark
	@./$(EXECUTABLE)

.PHONY: clean
clean:
	@rm -rfv $(EXECUTABLE)
