/*
 * ARM offline test
 *
 * Main control code for parking system
 *
 * Author: Attila Szasz
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "carpark.h"

/*
 * Return the node with minimum value
 */
struct tree_node * t_minvalnode(struct tree_node *node)
{
        struct tree_node* current = node;

        /* loop down to find the leftmost leaf */
        while (current && current->left_p != NULL)
                current = current->left_p;

        return current;
}

/*
 * Delete and return new root
 */
struct tree_node* t_delete(struct tree_node *root, uint64_t value)
{
        if (root == NULL)
                return root;

        if (value < root->numberplate)
                root->left_p = t_delete(root->left_p, value);

        else if (value > root->numberplate)
                root->right_p = t_delete(root->right_p, value);

        else {
                struct tree_node* temp;
                if (root->left_p == NULL) {
                         temp = root->right_p;
                         free(root);
                         return temp;
                } else if (root->right_p == NULL) {
                         temp = root->left_p;
                         free(root);
                         return temp;
                }

                struct tree_node *temp_node = t_minvalnode(root->right_p);
                root->numberplate = temp_node->numberplate;
                root->right_p = t_delete(root->right_p, temp_node->numberplate);

        }

       return root;
}

/*
 * Tree search algorithm
 */
struct tree_node * t_search(struct tree_node *root, uint64_t value)
{
        while (root) {
                if (root->numberplate == value)
                        return root;
                if (root->numberplate < value)
                        root = root->right_p;
                else
                        root = root->left_p;
        }

        return NULL;
}

/*
 * We use a pointer to a pointer to the root node.
 */
int t_insert(struct tree_node **root, uint64_t value, struct car_entry *data, unsigned int pos_index)
{
        while(*root) {
                if ((*root)->numberplate == value)
                        return TREE_ALRDY;
                if ((*root)->numberplate > value)
                        root = &((*root)->left_p);
                else
                        root = &((*root)->right_p);
        }
        if (!(*root = (struct tree_node*)malloc(sizeof(struct tree_node))))
                return TREE_NOMEM;

        (*root)->numberplate = value;

        (*root)->index = pos_index;

        (*root)->vehicle.numberplate   = data->numberplate;
        strncpy((*root)->vehicle.ownername, data->ownername, OWNERNAMELEN);
        (*root)->vehicle.entered = time(NULL);

        (*root)->left_p = NULL;
        (*root)->right_p = NULL;
        return TREE_SUCC;
}

/*
 * Recursive walk of the binary tree
 */
void t_walk(struct tree_node *root_p)
{
        if (!root_p)
                return;
        t_walk(root_p->left_p);
        printf(" %u ", (unsigned int)root_p->numberplate);
        t_walk(root_p->right_p);
}

void init_car_park(struct car_park **carpark_init) {
        struct free_segment* root_segment;
        root_segment = (struct free_segment*) malloc(sizeof(struct free_segment));
        if (!root_segment)
                return;

        root_segment->next = NULL;
        root_segment->prev = NULL;
        root_segment->start = 0;
        root_segment->end = MAXCARS-1;

        /* reserving space for the car park */
        (*carpark_init) = (struct car_park *)malloc(sizeof(struct car_park));
        if (!(*carpark_init))
                return;
        (*carpark_init)->capacity = MAXCARS;
        (*carpark_init)->allocated_slots = 0;
        (*carpark_init)->free_segment_root = root_segment;
        (*carpark_init)->tree_root = NULL;
}


int enter_car_park(struct car_park* cp, struct car_entry vehicle){
        unsigned int pos_index = 0;

        /* check capacity */
        if (cp->allocated_slots == cp->capacity) {
                return CP_NOSPACE;
        }

        /* manage free segments */
        struct free_segment * current = cp->free_segment_root;
        for(; current != NULL; current = current->next) {
                if (current->next == NULL) {
                        pos_index = current->start;
                        printf("allocating position: %d\n", pos_index);
                        current->start += 1;
                        /* Close the free segment when necessary */
                        if (current->start == current->end) {
                                if (current->prev)
                                        current->prev->next = NULL;
                                        free(current);
                        }
                }
        }
        /* insert into binary tree */
        t_insert(&cp->tree_root, vehicle.numberplate, &vehicle, pos_index);

        cp->allocated_slots += 1;

        return CP_SUCC;
}

int exit_car_park(struct car_park* cp, uint64_t numberplate) {
        struct tree_node *vehicleslot;

        /* retrieve vehicle */
        vehicleslot = t_search(cp->tree_root, numberplate);
        if (vehicleslot) {
                printf("position: %d\n", vehicleslot->index);
                printf("name: %s\n", vehicleslot->vehicle.ownername);
                double spent_in_park = time(NULL) - vehicleslot->vehicle.entered;
                printf("spent in park: %f sec\n", spent_in_park);
        }
        else
                return CP_ERR;

        /* manage car park structures */
        cp->tree_root = t_delete(cp->tree_root, numberplate);

        /* manage free segments */
        struct free_segment *insert_elem;

        struct free_segment * current = cp->free_segment_root;
        for(; current != NULL; current = current->next) {
                if (current->next &&
                    (vehicleslot->index < current->start) &&
                    (vehicleslot->index > current->next->end)) {
                        /* put new free segment in the middle of allocated */
                        insert_elem = (struct free_segment*)malloc(sizeof(struct free_segment));
                        if(!insert_elem)
                                 return CP_ERR;
                        insert_elem->start = vehicleslot->index;
                        insert_elem->end = insert_elem->start+1;
                        insert_elem->prev = current;
                        insert_elem->next = current->next;

                        current->next = insert_elem;
                        current->next->prev = insert_elem;
                        break;
                } else if (current->next &&
                          (vehicleslot->index < current->start) &&
                          (vehicleslot->index == current->next->end)) {
                        /* extend the right side of the left segment */
                        current->next->end += 1;
                } else if (current->next &&
                          (vehicleslot->index == current->start-1) &&
                          (vehicleslot->index > current->next->end)) {
                        /* extend the left side of the right segment */
                        current->start -= 1;
                } else if (!current->next &&
                           (vehicleslot->index == current->start-1)) {
                        /* only the root segment is there, extend if it is possilbe */
                        current->start -= 1;
                } else if (!current->next &&
                           (vehicleslot->index < current->start-1)) {
                        /* only the root segment is there, but need to create new */

                        insert_elem = (struct free_segment*)malloc(sizeof(struct free_segment));
                        if(!insert_elem)
                                 return CP_ERR;
                        insert_elem->start = vehicleslot->index;
                        insert_elem->end = insert_elem->start+1;
                        insert_elem->prev = current;
                        insert_elem->next = NULL;

                        current->next = insert_elem;
                        break;
                }

        }

        return CP_SUCC;
}

int main(int argc, char** argv)
{
        printf("ARM offline test solution (%s)\nCompiled on:\t%s %s\n\n", __FILE__, __DATE__, __TIME__);

        struct car_park *my_car_park;
        init_car_park(&my_car_park);

        struct car_entry first_car = { .numberplate = 0xdeadbeef, .ownername = "Attila Szasz" };
        enter_car_park(my_car_park, first_car);

        struct car_entry second_car = { .numberplate = 0xffadbeef, .ownername = "John Doe" };
        enter_car_park(my_car_park, second_car);

        struct car_entry third_car = { .numberplate = 0xdeadffef, .ownername = "Simon Segars" };
        enter_car_park(my_car_park, third_car);

        sleep(1);
        exit_car_park(my_car_park, (uint64_t) 0xdeadffef);
        sleep(1);
        exit_car_park(my_car_park, (uint64_t) 0xdeadbeef);

        struct car_entry fourth_car = { .numberplate = 0x13371337, .ownername = "Adam Lukacs" };
        enter_car_park(my_car_park, fourth_car);
        enter_car_park(my_car_park, third_car);

        return EXIT_SUCCESS;
}
